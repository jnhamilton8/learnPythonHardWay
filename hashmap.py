# aMap is a list of buckets
# each bucket has slots containing key/value pairs 

def new(num_buckets=236):
    """Initialises a Map with the given number of buckets (lists)."""
    aMap = []  # create empty list to hold the buckets
    # the buckets will hold the key value pairs
    for i in range(0, num_buckets):
        aMap.append([])
    return aMap

def hash_key(aMap, key):
    """Given a key this will create a number and then convert it to an index for the aMap's buckets."""
    # uses the built in hash function that converts string to a number 
    # e.g. hash = 2125708149 which is a large value, so % this by 236 to get
    # a smaller value where the bucket can go e.g. 2125708149 % 236 = 217
    # So we know the bucket will fit into the aMap list
    return hash(key) % len(aMap)

def get_bucket(aMap, key):
    """Given a key, find the bucket where it COULD go."""
    bucket_id = hash_key(aMap, key)
    return aMap[bucket_id]

def get_slot(aMap, key, default=None):
    """
    Returns the index, key and value of a slot found in a bucket.
    Returns -1, key and default (None if not set) when not found.
    """
    # uses get_bucket to find the bucket a key could be in
    bucket = get_bucket(aMap, key)

    # iterates through elements in bucket until a matching key is found
    for i, kv in enumerate(bucket):
        k, v = kv
        if key == k:
            return i, k, v

    return -1, key, default

def get(aMap, key, default=None):
    """Gets the value in a bucket for the given key, or the default"""
    # uses get_slot to get i, k, v and returns the value
    i, j, v = get_slot(aMap, key, default=default)
    return v

def set(aMap, key, value):
    """Sets the key to the value, replacing any existing value"""
    bucket = get_bucket(aMap, key)
    i, k, v = get_slot(aMap, key)

    if i >= 0:
        # the key exists, replace it - only one key allowed at a time
        bucket[i] = (key, value)
    else:
        # the key does not, append to create it
        bucket.append((key, value))

def delete(aMap, key):
    """Deletes the given key from the Map."""
    bucket = get_bucket(aMap, key)

    # iterates over the slots in the bucket (i.e. length of bucket)and if finds the key, deletes it
    for i in xrange(len(bucket)):
        k, v = bucket[i]
        if key == k:
            del bucket[i]
            break

def list(aMap):
    """Prints out what's in the map"""
    for bucket in aMap:
        if bucket:
            for k, v in bucket:
                print k, v







