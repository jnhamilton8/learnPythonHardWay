#formatter = "%r %r %r %r"

#print formatter % (1, 2, 3, 4)
#print formatter % ("one", "two", "three", "four")
#print formatter % (True, False, False, True)
#print formatter % (formatter, formatter, formatter, formatter)
#print formatter % (
#        "I had this thing.",
#        "That you could type up right.",
#        "But it didn't sing.",
#        "So I said goodnight.") 

#days = "Mon Tue Wed Thu Fri Sat Sun"
#months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

#print "Here are the days: ", days
#print "here are the months: ", months

#print """
#There's something going on here.
#With the three double quotes.
#We'll be able to type as much as we like.
#Even 4 mines if we want, or 5, or 6.
#"""

tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = '''
I'll do a list:
\t* Cat Food
\t* Fishies
\t* Catnip\n\t* Grass
'''

print tabby_cat
print persian_cat
print backslash_cat
print fat_cat

format = """
\tThis is a test\n
\tOver a few lines\n
\tWonder how it will print\n
"""

print "%s" % format

#while True:
#    for i in ["/", "-", "|", "\\", "|"]:
#        print "%s\r" % i

