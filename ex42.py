class Animal(object):
    
    def makes_a_noise(self, noise):
        print "The animal %s!" % noise

## Dog IS A animal
class Dog(Animal):

    def __init__(self, name):
        ## Dog HAS A name
        self.name = name

## Cat IS A animal
class Cat(Animal):
    
    def __init__(self, name):
        ## Cat HAS A name
        self.name = name

    def makes_a_noise(self, noise):
        print noise

## Person IS A object
class Person(object):

    def __init__(self, name):
        ## Person HAS A name
        self.name = name

        ## Person HAS A pet of some kind
        self.pet = None

## Employee IS A person
class Employee(Person):

    def __init__(self, name, salary):
        ## calls the Person super class constructor with name as arg
        super(Employee, self).__init__(name)
        ## Employee HAS A salary
        self.salary = salary

## Fish IS A object
class Fish(object):
    pass

## Salmon IS A fish
class Salmon(Fish):
    pass

## Halibut IS A fish
class Halibut(Fish):
    pass

## rover IS A Dog
rover = Dog("Rover")
rover.makes_a_noise("Barked")

## satan IS A Cat
satan = Cat("Satan")
satan.makes_a_noise("Cat does something special!")

## Mary IS A person
mary = Person("Mary")

## Mary's HAS A pet who is satan
mary.pet = satan

## Frank IS A employee, HAS A salary of 120000
frank = Employee("Frank", 120000)

## Frank HAS A pet who is rover
frank.pet = rover

## Flipper IS A fish
flipper = Fish()

## Crouse IS A Salmon
crouse = Salmon()

## Harry IS A Halibut
harry = Halibut()



