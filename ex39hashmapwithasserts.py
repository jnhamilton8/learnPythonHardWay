import hashmap
# this version uses the custom implementation of a dict (in hashmap.py)

#create a mapping of state to abbreviation
states = hashmap.new()
hashmap.set(states, 'Oregon', 'OR')
hashmap.set(states, 'Florida', 'FL')
hashmap.set(states, 'California', 'CA')
hashmap.set(states, 'New York', 'NY')
hashmap.set(states, 'Michigan', 'MI')

# create a basic set of states and some cities in them
cities = hashmap.new()
hashmap.set(cities, 'CA', 'San Francisco')
hashmap.set(cities, 'MI', 'Detriot')
hashmap.set(cities, 'FL', 'Jacksonville')

# add some more cities
hashmap.set(cities, 'NY', 'New York')
hashmap.set(cities, 'OR', 'Portland')

# print out some cities
print '-' * 10
assert hashmap.get(cities, 'NY'), "City not present in map"
assert hashmap.get(cities, 'OR'), "City not present in map"

# print some states
print '-' * 10
assert hashmap.get(states, 'Michigan'),"State not present in map" 
assert hashmap.get(states, 'Peanuts'), "State not present in map"

# do it by using the state then cities dict
print '-' * 10
assert hashmap.get(cities, hashmap.get(states, 'Michigan')), "State not present in map"
assert hashmap.get(cities, hashmap.get(states, 'Florida')), "State not present in map"

# print every state abbreviation
print '-' * 10
hashmap.list(states)

# print every city in state
print '-' * 10
hashmap.list(cities)

print '-' * 10
state = hashmap.get(states, 'Texas')

if not state:
    print "Sorry, no Texas."

# default values using ||= with the nil result
# can you do this on one line?
city = hashmap.get(cities, 'TX', 'Does not exist')
print "The city for the state 'TX' is: %s" % city














