my_name = "Jane Hamilton"
my_age = 39
my_height = 74.0 #inches
my_weight = 115 #lbs
my_eyes = "Blue"
my_teeth = "white"
my_hair = "Brown"

print "Let's take about %s." % my_name
print "He's %d inches tall." % my_height
print "He's %d pounds heavy." % my_weight
print "He's got %s eyes and %s hair." % (my_eyes, my_hair)

print "If I add %d, %d, and %d I get %d." % (my_age, my_height, my_weight, my_age + my_height + my_weight)

cm_value = 2.54
kilo_value = 2.20462

print "Convering the inches to cm:"
print "My height in inches: %s " % my_height
print "My height in cm: %s" % (my_height * cm_value)

print "My weight in pounds: %.2f" % my_weight
converting_to_kilos = round(my_weight / kilo_value)
print "My weight in kilograms: %.2f" % converting_to_kilos  

