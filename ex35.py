# Example of using as.  This automatically closes the file too. 
with open('testtext.txt', 'w') as f:
    f.write("Hi there!")

# assert. Evaluates a condition. If true, does nothing.  If false, raises an AssertionError exception
a = 3
b = 4
assert a + b == 7 # will fail if set to e.g. 8

# an example of a python class

class Customer(object):

    """A customer of ABC Bank with a checking account. Customers have the
    following properties:

    Attributes:
        name: A string representing the customer's name.
        balance: A float tracking the current balance of the customer's account.
    """
    
    def __init__(self, name, balance):
        self.name = name
        self.balance = balance

    def withdraw(self, amount):
        """Return the balance remaining after withdrawing *amount* dollars."""
        if amount > self.balance:
             raise RuntimeError("Amount greater than available balance")
        self.balance -= amount
        return self.balance
    
    def deposit(self, amount):
        """Return the balance remaining after depositing *amount*
        dollars."""
        self.balance += amount
        return self.balance
   
jane = Customer('jane', 1000)
balance = jane.deposit(100)
print balance

# example of a lambda
b = (lambda x, y : x + y)
result = b(5, 7)
print result

